import 'package:flutter/material.dart';
import 'package:mmodule3/login.dart';
import 'package:mmodule3/dashboard.dart';
import 'package:mmodule3/edit.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Dashboard'),
          centerTitle: true,
        ),
        body: Column(
          children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(
                fit: StackFit.expand,
                clipBehavior: Clip.none,
              ),
            ),
            const Text('Press the button below to edit your profile'),
            ElevatedButton(
              child: const Text("Edit"),
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const UserEdit()))
              },
            ),
          ],
        ));
  }
}
