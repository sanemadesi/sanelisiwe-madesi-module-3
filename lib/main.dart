import 'package:flutter/material.dart';
import 'package:mmodule3/login.dart';
// import 'package:mmodule3/registration.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      home: Login(),
      theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.teal)
              .copyWith(secondary: Colors.tealAccent),
          scaffoldBackgroundColor: Color.fromARGB(255, 223, 219, 219)),
    );
  }
}
